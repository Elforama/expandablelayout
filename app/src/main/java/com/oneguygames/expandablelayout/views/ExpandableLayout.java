package com.oneguygames.expandablelayout.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.oneguygames.expandablelayout.R;

/**
 * Created by jonathanmuller on 6/11/15.
 */
public class ExpandableLayout extends LinearLayout {

    private static final String TAG = "ExpandableLayout";

    private int viewGroupId;
    private ViewGroup viewGroup;

    private RelativeLayout header;

    OnExpandListener onExpandListener;
    OnCollapseListener onCollapseListener;

    private boolean isExpanded = false;

    public ExpandableLayout(Context context) {
        super(context);
        init();
    }

    public ExpandableLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ExpandableLayout, 0, 0);

        viewGroupId = 0;
        viewGroupId = a.getResourceId(R.styleable.ExpandableLayout_viewgroup, 0);



        a.recycle();
    }

    private void init() {
        inflate(getContext(), R.layout.expandable_layout, this);
        header = (RelativeLayout)findViewById(R.id.header_container);



        header.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                isExpanded = !isExpanded;
                if (isExpanded) {
                    //viewGroup.setVisibility(View.VISIBLE);
                    onExpandListener.onExpanded(view);
                } else {
                    //viewGroup.setVisibility(View.GONE);
                    onCollapseListener.onCollapsed(view);
                }
            }
        });
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if(viewGroupId != 0) {
            Log.d(TAG, "Found viewgroup id");
            viewGroup = (ViewGroup)getRootView().findViewById(viewGroupId);
            if(viewGroup == null){
                throw new IllegalArgumentException(
                        "The content attribute must refer to an"
                                + " existing child.");
            }else
                Log.d(TAG, "FOUND viewgroup");
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        if (!isExpanded) {
            viewGroup.setVisibility(View.GONE);

        } else {
            viewGroup.setVisibility(View.VISIBLE);
        }


        // Then let the usual thing happen
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public ExpandableLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnExpandListener(OnExpandListener listener) {
        onExpandListener = listener;
    }

    public void setCollapsedListener(OnCollapseListener listener) {
        onCollapseListener = listener;
    }

    public static abstract class OnExpandListener{
        public abstract void onExpanded(View view);
    }

    public static abstract class OnCollapseListener{
        public abstract void onCollapsed(View view);
    }
}

